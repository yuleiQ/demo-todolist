// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// ReactDOM.render(<App />, document.getElementById('root'));



import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './containers/App'
import todoApp from './reducers'

let store = createStore(todoApp)//createStore函数接受另一个函数作为参数，返回新生成的 Store 对象。

let rootElement = document.getElementById('root')
render(
    <Provider store={store}>
        <App />
    </Provider>,
    rootElement
)